# README #

### Базовые команды ###

* git status - Вывод состояния рабочего каталога
* git log - Вывод истории коммитов

### Создание и сохранение коммита на удаленный репозиторий ###

* git add . - Добавление содержимого файла в индекс
* git commit - Запись изменений в репозиторий
* git push origin master - Обновление внешних ссылок и связанных объектов (ветки master)

### Git команды для инициализации репозитория ###

* git init
* git config user.name Valery Malyshev
* git config user.email vmalyshev@wolf10.ru